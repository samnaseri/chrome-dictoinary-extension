  // Set up the context menus
  chrome.contextMenus.create({
	"title": "Search Merriam Webster for this",
	"contexts": ["selection"],
	"onclick" : function(e) {
	  if (e.selectionText) {
		var dicUrl = "http://www.merriam-webster.com/dictionary/" + encodeURI(e.selectionText) ;
		chrome.tabs.create({"url" : dicUrl });
	  }
	}
  }); 
